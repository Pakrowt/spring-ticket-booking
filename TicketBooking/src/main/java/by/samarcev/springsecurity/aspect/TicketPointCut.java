package by.samarcev.springsecurity.aspect;

import org.aspectj.lang.annotation.Pointcut;

public class TicketPointCut {

    @Pointcut(value = "execution(public * purchaseTicket(..))")
    public void purchaseTicketPointCut() {
    }
}
