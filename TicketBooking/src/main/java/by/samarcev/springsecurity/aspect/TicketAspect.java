package by.samarcev.springsecurity.aspect;

import by.samarcev.springsecurity.model.Ticket;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Slf4j
public class TicketAspect {

    @Before("by.samarcev.springsecurity.aspect.TicketPointCut.purchaseTicketPointCut()")
    private void beforePurchaseTicketAdvice() {
        log.info("purchase ticket method started! ");
    }

    @AfterReturning(pointcut = "by.samarcev.springsecurity.aspect.TicketPointCut.purchaseTicketPointCut()",
            returning = "ticket")
    private void afterPurchaseTicketAdvice(Ticket ticket) {
        log.info("purchase ticket method after successfully executing");
    }

    @AfterThrowing(pointcut = "by.samarcev.springsecurity.aspect.TicketPointCut.purchaseTicketPointCut()",
            throwing = "exception")
    private void afterPurchaseTicketWhereThrowingExceptionAdvice(Throwable exception) {
        log.error("An error has occurred while executing the method - {}", exception.getMessage());
    }
}
