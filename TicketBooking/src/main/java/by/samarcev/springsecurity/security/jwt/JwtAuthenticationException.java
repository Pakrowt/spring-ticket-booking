package by.samarcev.springsecurity.security.jwt;

import org.springframework.http.HttpStatus;

import javax.naming.AuthenticationException;

public class JwtAuthenticationException extends AuthenticationException {

    public JwtAuthenticationException(String msg, HttpStatus httpStatus) {
        super(msg);
    }

    public JwtAuthenticationException(String msg) {
        super(msg);
    }
}
