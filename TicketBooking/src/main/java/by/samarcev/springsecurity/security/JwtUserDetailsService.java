package by.samarcev.springsecurity.security;

import by.samarcev.springsecurity.model.User;
import by.samarcev.springsecurity.security.jwt.JwtUser;
import by.samarcev.springsecurity.security.jwt.JwtUserFactory;
import by.samarcev.springsecurity.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> maybeUser = userService.findByLogin(username);
        if (maybeUser.isEmpty()) {
            throw new UsernameNotFoundException("User with username " + username + " not found");
        }
        JwtUser jwtUser = JwtUserFactory.create(maybeUser.get());
        log.info("In loadUserByUsername user with login - {} successfully loaded", username);
        return jwtUser;
    }
}
