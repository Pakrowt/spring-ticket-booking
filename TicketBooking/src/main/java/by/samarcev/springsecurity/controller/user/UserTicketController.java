package by.samarcev.springsecurity.controller.user;

import by.samarcev.springsecurity.converter.TicketConverter;
import by.samarcev.springsecurity.dto.InformationTicketDto;
import by.samarcev.springsecurity.dto.TicketDto;
import by.samarcev.springsecurity.model.Ticket;
import by.samarcev.springsecurity.service.TicketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/users")
@Api(value = "Контроллер для покупки пользователем билетов")
@RequiredArgsConstructor
public class UserTicketController {

    private final TicketService ticketService;
    private final TicketConverter ticketConverter;

    @PostMapping("/tickets/{cardId}")
    @ApiOperation(value = "Метод для покупки билетов", response = Ticket.class)
    public ResponseEntity<Ticket> purchaseTicket(@PathVariable("cardId") Long cardId, @RequestBody TicketDto ticket) {
        Ticket entity = ticketConverter.dtoToEntity(ticket);
        Ticket ticket1 = ticketService.purchaseTicket(entity, cardId);
        return new ResponseEntity<>(ticket1, HttpStatus.CREATED);
    }

    @GetMapping("/tickets/{userId}")
    @ApiOperation(value = "Метод для получения всех купленных билетов пользователя")
    public ResponseEntity<List<TicketDto>> getAllPurchaseTickets(@PathVariable(value = "userId") Long userId) {
        List<TicketDto> allTickets = ticketService.getAllTicketsByUserId(userId);
        return new ResponseEntity<>(allTickets, HttpStatus.OK);
    }

    @GetMapping("/ticketsinformations/{ticketId}")
    @ApiOperation(value = "Метод для получения подробной информации о купленном билете")
    public ResponseEntity<InformationTicketDto> getTicketWithInformationAboutTrip(@PathVariable(value = "ticketId")
                                                                                          Long ticketId) {
        InformationTicketDto ticket = ticketService.findTicketWithAllInformationAboutTrip(ticketId);
        return new ResponseEntity<>(ticket, HttpStatus.OK);
    }
}
