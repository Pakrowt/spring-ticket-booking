package by.samarcev.springsecurity.controller.admin;

import by.samarcev.springsecurity.model.BusTerminal;
import by.samarcev.springsecurity.service.BusTerminalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/admin")
@Slf4j
@Api(value = "Контроллера администратора для работы с автобусными вокзалами (CRUD-operations)")
@RequiredArgsConstructor
public class AdminBusTerminalController {

    private final BusTerminalService busTerminalService;

    @GetMapping(value = "/terminals")
    @ApiOperation(value = "Метод для получения всех автобусных вокзалов", response = BusTerminal.class)
    public ResponseEntity<List<BusTerminal>> getAllBusTerminals(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "3") Integer pageSize,
            @RequestParam(defaultValue = "country") String sortBy) {
        List<BusTerminal> terminals = busTerminalService.getAllBusTerminal(pageNo, pageSize, sortBy);
        return new ResponseEntity<>(terminals, HttpStatus.OK);
    }

    @PostMapping(value = "/terminals")
    @ApiOperation(value = "Метод для создания автобусного вокзала")
    public ResponseEntity<String> createBusTerminal(@RequestBody BusTerminal busTerminal) {
        busTerminalService.addBusTerminal(busTerminal);
        return new ResponseEntity<>(("Bus Terminal " + busTerminal.getCity() + " successfully created!"),
                HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/terminals/{city}")
    @ApiOperation(value = "Метод для удаления автобусного вокзала по городу")
    public ResponseEntity<String> removeBusTerminal(@PathVariable("city") String city) {
        busTerminalService.deleteBusTerminal(city);
        return new ResponseEntity<>(("Bus Terminal " + city + " successfully deleted!"), HttpStatus.OK);
    }

    @PatchMapping(value = "/terminals/{city}")
    @ApiOperation(value = "Метод для изменения информации об автобусном вокзале", response = BusTerminal.class)
    public ResponseEntity<BusTerminal> modifyBusTerminal(@PathVariable("city") String city,
                                                         @RequestBody BusTerminal busTerminal) {
        busTerminalService.modifyBusTerminal(city, busTerminal);
        return new ResponseEntity<>(busTerminal, HttpStatus.OK);
    }
}
