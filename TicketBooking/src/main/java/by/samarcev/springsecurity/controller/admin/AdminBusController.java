package by.samarcev.springsecurity.controller.admin;

import by.samarcev.springsecurity.model.Bus;
import by.samarcev.springsecurity.service.BusService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/admin")
@Slf4j
@Api(value = "Контроллер администратора для управления автобусами (CRUD-operations)")
@RequiredArgsConstructor
public class AdminBusController {

    private final BusService busService;

    @GetMapping("/buses/{busNumber}")
    @ApiOperation(value = "Метод для получения автобуса по его номеру", response = Bus.class)
    public ResponseEntity<Bus> getBusByNumber(@PathVariable("busNumber") String busNumber) {
        Bus bus = busService.getBusByNumber(busNumber);
        return new ResponseEntity<>(bus, HttpStatus.OK);
    }

    @GetMapping("/buses")
    @ApiOperation(value = "Метод для получения всех автобуса", response = Bus.class)
    public ResponseEntity<List<Bus>> getAllBuses(@RequestParam(defaultValue = "0") Integer pageNo,
                                                 @RequestParam(defaultValue = "3") Integer pageSize,
                                                 @RequestParam(defaultValue = "capacity") String sortBy) {
        List<Bus> buses = busService.getAllBus(pageNo, pageSize, sortBy);
        return new ResponseEntity<>(buses, HttpStatus.OK);
    }

    @PostMapping("/buses")
    @ApiOperation(value = "Метод для создания автобуса")
    public ResponseEntity<String> createBus(@RequestBody Bus bus) {
        busService.addBus(bus);
        return new ResponseEntity<>(("Bus " + bus.getId() + " successfully created!"), HttpStatus.CREATED);
    }

    @DeleteMapping("/buses/{busNumber}")
    @ApiOperation(value = "Метод для удаления автобуса по его номеру")
    public ResponseEntity<String> removeBus(@PathVariable("busNumber") String busNumber) {
        busService.deleteBus(busNumber);
        return new ResponseEntity<>(("Bus with busNumber: " + busNumber + " successfully deleted!"), HttpStatus.OK);
    }

    @PatchMapping("/buses/{busNumber}")
    @ApiOperation(value = "Метод для обновления информации автобуса", response = Bus.class)
    public ResponseEntity<Bus> modifyBus(@PathVariable("busNumber") String busNumber, @RequestBody Bus bus) {
        busService.modifyBus(busNumber, bus);
        return new ResponseEntity<>(bus, HttpStatus.OK);
    }

}
