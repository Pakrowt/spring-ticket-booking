package by.samarcev.springsecurity.controller;

import by.samarcev.springsecurity.model.User;
import by.samarcev.springsecurity.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1")
@Api(value = "Контроллер для пользователей")
@RequiredArgsConstructor
public class UserRegisterController {

    private final UserService userService;

    @PostMapping(value = "/register")
    @ApiOperation(value = "Метод для создания пользователя")
    public ResponseEntity<String> createUser(@RequestBody User user) {
        if (userService.findByLogin(user.getLogin()).isPresent()) {
            return new ResponseEntity<>(" User with that username exists in the database", HttpStatus.BAD_REQUEST);
        }
        userService.register(user);
        return new ResponseEntity<>("User created successfully!", HttpStatus.CREATED);
    }
}
