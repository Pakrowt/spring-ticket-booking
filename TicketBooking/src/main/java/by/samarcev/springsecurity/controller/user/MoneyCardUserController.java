package by.samarcev.springsecurity.controller.user;

import by.samarcev.springsecurity.converter.MoneyCardConverter;
import by.samarcev.springsecurity.dto.MoneyCardDto;
import by.samarcev.springsecurity.dto.MoneyCardReplenishmentDto;
import by.samarcev.springsecurity.model.MoneyCard;
import by.samarcev.springsecurity.service.MoneyCardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping("/api/v1/users")
@Slf4j
@Api(value = "Контроллер для работы пользователя с банковской картой")
@RequiredArgsConstructor
public class MoneyCardUserController {

    private final MoneyCardService moneyCardService;
    private final MoneyCardConverter moneyCardConverter;

    @PostMapping("/cards")
    @ApiOperation(value = "Метод для добавления новой карты")
    public ResponseEntity<String> addMoneyCard(@RequestBody MoneyCardDto moneyCardDto) {
        MoneyCard moneyCard = moneyCardConverter.dtoToEntity(moneyCardDto);
        moneyCardService.addBankAccount(moneyCard);
        return new ResponseEntity<>(("Money card - " + moneyCard.getNameCard() + " successfully added!"),
                HttpStatus.CREATED);
    }

    @PatchMapping("/cards/{cardId}")
    @ApiOperation(value = "Метод для пополнения счета определенной банковской карты", response = MoneyCardDto.class)
    public ResponseEntity<MoneyCardDto> putMoneyIntoAccount(@PathVariable("cardId") Long cardId,
                                                            @RequestBody MoneyCardReplenishmentDto card) {
        BigDecimal amount = card.getAmount();
        moneyCardService.putMoneyIntoAccount(cardId, amount);
        MoneyCard moneyCard = moneyCardService.findCardById(cardId);
        MoneyCardDto moneyCardDto = moneyCardConverter.entityToDto(moneyCard);
        return new ResponseEntity<>(moneyCardDto, HttpStatus.OK);
    }

    @GetMapping("/cards/{cardId}")
    @ApiOperation(value = "Метод для проверки баланса по номеру карты")
    public ResponseEntity<String> checkingAccountBalance(@PathVariable("cardId") Long cardId) {
        BigDecimal balance = moneyCardService.checkingAccountBalance(cardId);
        return new ResponseEntity<>(("balance : " + balance), HttpStatus.OK);
    }
}

