package by.samarcev.springsecurity.controller.user;

import by.samarcev.springsecurity.dto.UserInformationDto;
import by.samarcev.springsecurity.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1/users")
@RequiredArgsConstructor
public class UserInformationController {

    private final UserService userService;

    @PatchMapping("/informations/{login}")
    @ApiOperation(value = "Метод для обновления персональных данных пользователя")
    public ResponseEntity<String> updateUserInformation(@RequestBody UserInformationDto userInformationDto,
                                                        @PathVariable("login") String login) {
        userService.updateUserInformation(login, userInformationDto.getFirstName(), userInformationDto.getLastName());
        return new ResponseEntity<>("User successfully updated information!", HttpStatus.OK);
    }
}
