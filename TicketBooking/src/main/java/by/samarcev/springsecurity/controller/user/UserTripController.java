package by.samarcev.springsecurity.controller.user;

import by.samarcev.springsecurity.converter.TripConverter;
import by.samarcev.springsecurity.dto.TripDto;
import by.samarcev.springsecurity.model.Trip;
import by.samarcev.springsecurity.service.TripService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/users")
@Slf4j
@Api(value = "Контроллер для отображения пользователю доступных поездок")
@RequiredArgsConstructor
public class UserTripController {

    private final TripService tripService;
    private final TripConverter tripConverter;

    @GetMapping("/trips")
    @ApiOperation(value = "Метод для получения всех доступных поездок", response = TripDto.class)
    public ResponseEntity<List<TripDto>> getAllTrips(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "3") Integer pageSize,
            @RequestParam(defaultValue = "departureDate") String sortBy) {
        List<Trip> allTrips = tripService.getAllTrips(pageNo, pageSize, sortBy);
        List<TripDto> tripDtos = tripConverter.entityToDto(allTrips);
        return new ResponseEntity<>(tripDtos, HttpStatus.OK);
    }

    @GetMapping("/trips/departures/locations/{departureLocation}")
    @ApiOperation(value = "Метод для получения всех доступных поездок по месту отправления", response = TripDto.class)
    public ResponseEntity<List<TripDto>> getTripsByDepartureLocation(@PathVariable(value = "departureLocation")
                                                                             String departureLocation) {
        List<Trip> allTripsByDepartureLocation = tripService.getAllTripsByDepartureLocation(departureLocation);
        List<TripDto> tripDtos = tripConverter.entityToDto(allTripsByDepartureLocation);
        return new ResponseEntity<>(tripDtos, HttpStatus.OK);
    }

    @GetMapping("/trips/departures/{departureLocation}/{departureDate}")
    @ApiOperation(value = "Метод для получения всех доступных поездок по месту отправления и дате отправления",
            response = TripDto.class)
    public ResponseEntity<List<TripDto>> getTripsByDepartureLocationAndDepartureDate(
            @PathVariable(value = "departureLocation") String departureLocation,
            @PathVariable(value = "departureDate") @DateTimeFormat(pattern = "yyyy-MM-dd:HH:mm:ss")
                    LocalDateTime departureDate) {
        List<Trip> allTrips = tripService.getAllTripsByDepartureLocationAndDepartureDate(departureLocation, departureDate);
        List<TripDto> tripDtos = tripConverter.entityToDto(allTrips);
        return new ResponseEntity<>(tripDtos, HttpStatus.OK);
    }

    @GetMapping("/trips/departures/dates/{departureDate}")
    @ApiOperation(value = "Метод для получения всех доступных поездок по дате отправления", response = TripDto.class)
    public ResponseEntity<List<TripDto>> getTripsByDepartureDate(
            @DateTimeFormat(pattern = "yyyy-MM-dd:HH:mm:ss") @PathVariable("departureDate") LocalDateTime date) {
        List<Trip> allByDepartureDate = tripService.getAllByDepartureDate(date);
        List<TripDto> tripDtos = tripConverter.entityToDto(allByDepartureDate);
        return new ResponseEntity<>(tripDtos, HttpStatus.OK);
    }

    @GetMapping("/trips/arrivals/locations/{arrivalLocation}")
    @ApiOperation(value = "Метод для получения всех доступных поездок по месту прибытия", response = TripDto.class)
    public ResponseEntity<List<TripDto>> getTripsByArrivalLocation(@PathVariable("arrivalLocation") String arrivalLocation) {
        List<Trip> allTripsByArrivalLocation = tripService.getAllTripsByArrivalLocation(arrivalLocation);
        List<TripDto> tripDtos = tripConverter.entityToDto(allTripsByArrivalLocation);
        return new ResponseEntity<>(tripDtos, HttpStatus.OK);
    }

    @GetMapping("/trips/arrivals/{arrivalLocation}/{arrivalDate}")
    @ApiOperation(value = "Метод для получения всех доступных поездок по месту прибытия и дате", response = TripDto.class)
    public ResponseEntity<List<TripDto>> getTripsByArrivalLocationAndArrivalDate(
            @PathVariable(value = "arrivalLocation") String arrivalLocation,
            @PathVariable(value = "arrivalDate") @DateTimeFormat(pattern = "yyyy-MM-dd:HH:mm:ss")
                    LocalDateTime arrivalDate) {
        List<Trip> allTrips = tripService.getAllTripsByArrivalLocationAndArrivalDate(arrivalLocation, arrivalDate);
        List<TripDto> tripDtos = tripConverter.entityToDto(allTrips);
        return new ResponseEntity<>(tripDtos, HttpStatus.OK);
    }

    @GetMapping("/trips/buses/{busNumber}")
    @ApiOperation(value = "Метод для получения всех доступных поездок по месту номеру автобуса", response = TripDto.class)
    public ResponseEntity<TripDto> getTripsByBus(@PathVariable("busNumber") String busNumber) {
        Trip tripByBusId = tripService.getTripByBusId(busNumber);
        TripDto tripDto = tripConverter.entityToDto(tripByBusId);
        return new ResponseEntity<>(tripDto, HttpStatus.OK);
    }


}
