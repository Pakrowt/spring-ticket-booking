package by.samarcev.springsecurity.controller.admin;

import by.samarcev.springsecurity.converter.AdminUserConverter;
import by.samarcev.springsecurity.dto.AdminUserDto;
import by.samarcev.springsecurity.model.User;
import by.samarcev.springsecurity.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v1/admin")
@Api(value = "Контроллер для администратора")
@Slf4j
@RequiredArgsConstructor
public class AdminUserController {

    private final UserService userService;
    private final AdminUserConverter adminUserConverter;

    @GetMapping(value = "/users/{login}")
    @ApiOperation(value = "Возвращает пользователя по указанному логину", response = AdminUserDto.class)
    public ResponseEntity<Optional<AdminUserDto>> getUserByLogin(@PathVariable(name = "login") String login) {
        Optional<User> user = userService.findByLogin(login);
        if (user.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        AdminUserDto adminUserDto = adminUserConverter.convertToDto(user.get());
        return new ResponseEntity(adminUserDto, HttpStatus.OK);
    }

    @DeleteMapping(value = "users/{login}")
    @ApiOperation(value = "Удаляет пользователя по указанному логину")
    public ResponseEntity<String> deleteUserByLogin(@PathVariable(name = "login") String login) {
        userService.delete(login);
        return new ResponseEntity<>(("User with login " + login + " was deleted!"), HttpStatus.OK);
    }

}


