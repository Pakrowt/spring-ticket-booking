package by.samarcev.springsecurity.controller.admin;

import by.samarcev.springsecurity.converter.TripConverter;
import by.samarcev.springsecurity.dto.TripDto;
import by.samarcev.springsecurity.service.TripService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1/admin")
@Api(value = "Контроллер администратора для управления поездками (CRUD-operations)")
@RequiredArgsConstructor
public class AdminTripController {

    public final TripService tripService;
    public final TripConverter tripConverter;

    @PostMapping("/trips")
    @ApiOperation(value = "Метод для создания поездки")
    public ResponseEntity<String> createTrip(@RequestBody TripDto tripDto) {
        tripService.addTrip(tripDto);
        return new ResponseEntity<>(("Trip with id" + tripDto.getId() + " successfully created!"), HttpStatus.CREATED);
    }

    @DeleteMapping("/trips/{id}")
    @ApiOperation(value = "Метод для удаления поездки по айдишнику")
    public ResponseEntity<String> removeTrip(@PathVariable("id") Long id) {
        tripService.deleteTrip(id);
        return new ResponseEntity<>(("Trip with id: " + id + " successfully deleted!"), HttpStatus.OK);
    }

    @PatchMapping("/trips/{id}")
    @ApiOperation(value = "Метод для изменения данных поездки по айдишнику", response = TripDto.class)
    public ResponseEntity<TripDto> modifyTrip(@PathVariable("id") Long id, @RequestBody TripDto tripDto) {
        tripService.modifyTrip(id, tripDto);
        return new ResponseEntity<>(tripDto, HttpStatus.OK);
    }


}

