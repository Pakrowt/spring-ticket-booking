package by.samarcev.springsecurity.service;

import by.samarcev.springsecurity.model.Bus;

import java.util.List;

public interface BusService {

    Bus getBusByNumber(String busNumber);

    List<Bus> getAllBus(Integer pageNo, Integer pageSize,
                        String sortBy);

    void addBus(Bus bus);

    void deleteBus(String busNumber);

    void modifyBus(String busNumber, Bus bus);
}
