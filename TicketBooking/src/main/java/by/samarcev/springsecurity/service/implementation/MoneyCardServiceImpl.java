package by.samarcev.springsecurity.service.implementation;

import by.samarcev.springsecurity.exception.NotFoundException;
import by.samarcev.springsecurity.model.MoneyCard;
import by.samarcev.springsecurity.repository.MoneyCardRepository;
import by.samarcev.springsecurity.service.MoneyCardService;
import by.samarcev.springsecurity.util.MoneyCardUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class MoneyCardServiceImpl implements MoneyCardService {

    private final MoneyCardRepository moneyCardRepository;

    @Override
    public void addBankAccount(MoneyCard moneyCard) {
        moneyCardRepository.save(moneyCard);
        findCardById(moneyCard.getId());
        log.info("money card - {} successfully added!", moneyCard.getNameCard());
    }

    @Override
    public void putMoneyIntoAccount(Long cardId, BigDecimal amount) {
        MoneyCard moneyCard = findCardById(cardId);
        moneyCard.setBalance(MoneyCardUtil.addMoney(moneyCard.getBalance(), amount));
        log.info("Card account replenishment - {} in the amount - {}", cardId, amount);
        moneyCardRepository.save(moneyCard);
    }

    @Override
    public BigDecimal checkingAccountBalance(Long cardId) {
        log.info("check balance - {}", cardId);
        return moneyCardRepository.checkingBalance(cardId);
    }

    public MoneyCard findCardById(Long cardId) {
        Optional<MoneyCard> moneyCard = moneyCardRepository.findById(cardId);
        if (moneyCard.isEmpty()) {
            log.error("card - {} not found", cardId);
            throw new NotFoundException("money card", cardId);
        }
        log.info("Card account - {} found", cardId);
        return moneyCard.get();
    }
}
