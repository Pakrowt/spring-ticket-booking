package by.samarcev.springsecurity.service;

import by.samarcev.springsecurity.model.MoneyCard;

import java.math.BigDecimal;

public interface MoneyCardService {

    void addBankAccount(MoneyCard moneyCard);

    void putMoneyIntoAccount(Long cardId, BigDecimal amount);

    BigDecimal checkingAccountBalance(Long cardId);

    MoneyCard findCardById(Long cardId);
}
