package by.samarcev.springsecurity.service;

import by.samarcev.springsecurity.dto.InformationTicketDto;
import by.samarcev.springsecurity.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    void register(User user);

    Optional<User> findByLogin(String login);

    void delete(String login);

    void updateUserInformation(String login, String firstName, String lastName);

    List<User> getAll();
}
