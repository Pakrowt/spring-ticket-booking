package by.samarcev.springsecurity.service.implementation;

import by.samarcev.springsecurity.converter.TripConverter;
import by.samarcev.springsecurity.dto.TripDto;
import by.samarcev.springsecurity.exception.TripNotFoundException;
import by.samarcev.springsecurity.model.Trip;
import by.samarcev.springsecurity.repository.BusRepository;
import by.samarcev.springsecurity.repository.BusTerminalRepository;
import by.samarcev.springsecurity.repository.TripRepository;
import by.samarcev.springsecurity.service.TripService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class TripServiceImpl implements TripService {

    private final TripRepository tripRepository;
    private final TripConverter tripConverter;
    private final BusRepository busRepository;
    private final BusTerminalRepository busTerminalRepository;

    @Override
    public List<Trip> getAllTripsByArrivalLocation(String arrivalLocation) {
        Optional<List<Trip>> trips = tripRepository.findAllByArrivalLocation_City(arrivalLocation);
        if (trips.get().isEmpty()) {
            log.error("trips with arrival location - {} not found ", arrivalLocation);
            throw new TripNotFoundException(("arrival location : " + arrivalLocation));
        }
        return trips.get();
    }

    @Override
    public List<Trip> getAllTripsByArrivalLocationAndArrivalDate(String arrivalLocation, LocalDateTime arrivalDate) {
        Optional<List<Trip>> trips = tripRepository.findAllByArrivalLocation_CityAndArrivalDate(arrivalLocation, arrivalDate);
        if (trips.get().isEmpty()) {
            log.error("trips with arrival location - {} and arrival date - {} not found ", arrivalLocation, arrivalDate);
            throw new TripNotFoundException(("arrival location : " + arrivalLocation + " and arrival date : " + arrivalDate));
        }
        return trips.get();
    }

    @Override
    public List<Trip> getAllTripsByDepartureLocation(String departureLocation) {
        Optional<List<Trip>> trips = tripRepository.findAllByDepartureLocation_City(departureLocation);
        if (trips.get().isEmpty()) {
            log.error("trips with departure location - {} not found ", departureLocation);
            throw new TripNotFoundException(("departure location : " + departureLocation));
        }
        return trips.get();
    }

    @Override
    public List<Trip> getAllTripsByDepartureLocationAndDepartureDate(String departureLocation, LocalDateTime departureDate) {
        Optional<List<Trip>> trips = tripRepository.findAllByDepartureLocation_CityAndDepartureDate(departureLocation,
                departureDate);
        if (trips.get().isEmpty()) {
            log.error("trips with departure location - {} and departure date - {} not found ",
                    departureLocation, departureDate);
            throw new TripNotFoundException(("departure location : " + departureLocation + " and departure date : "
                    + departureDate));
        }
        return trips.get();
    }

    @Override
    public Trip getTripByBusId(String busNumber) {
        Optional<Trip> trip = tripRepository.findByBusId(busNumber);
        if (trip.isEmpty()) {
            log.error("trip with bus number - {} not found ", busNumber);
            throw new TripNotFoundException(("bus number : " + busNumber));
        }
        return trip.get();

    }

    @Override
    public List<Trip> getAllTrips(Integer pageNo, Integer pageSize, String sortBy) {
        PageRequest pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        log.info("get all trips");
        Page<Trip> allTrips = tripRepository.findAllBy(pageable);
        if (allTrips.hasNext()) {
            return allTrips.getContent();
        } else
            return new ArrayList<>();
    }

    @Override
    public List<Trip> getAllByDepartureDate(LocalDateTime departureDate) {
        Optional<List<Trip>> trips = tripRepository.findAllByDepartureDate(departureDate);
        if (trips.get().isEmpty()) {
            log.error("trip with departure date - {} not found ", departureDate);
            throw new TripNotFoundException(("departure date number : " + departureDate));
        }
        return trips.get();
    }

    @Override
    public void addTrip(TripDto tripDto) {
        Trip trip = tripConverter.dtoToEntity(tripDto);
        tripRepository.save(trip);
        findTripById(trip.getId());
        log.info("trip - {} successfully created!", tripDto);
    }

    @Override
    public void deleteTrip(Long id) {
        findTripById(id);
        log.info("trip with id - {} successfully deleted", id);
        tripRepository.deleteById(id);
    }

    @Override
    public void modifyTrip(Long id, TripDto tripDto) {
        Trip trip = findTripById(id);
        trip.setDepartureDate(tripDto.getDepartureDate());
        trip.setDepartureLocation(busTerminalRepository.getById(tripDto.getDepartureLocation()));
        trip.setArrivalDate(tripDto.getArrivalDate());
        trip.setArrivalLocation(busTerminalRepository.getById(tripDto.getArrivalLocation()));
        trip.setBus(busRepository.getById(tripDto.getBusId()));
        trip.setStatus(tripDto.getStatus());
        log.info("trip {} successfully updated!", trip);
        tripRepository.save(trip);
    }

    public Trip findTripById(Long tripId) {
        Optional<Trip> trip = tripRepository.findById(tripId);
        if (trip.isEmpty()) {
            log.error("trip with id - {} not found!", tripId);
            throw new RuntimeException("Trip not found!");
        }
        log.info("trip with id - {} successfully found!", tripId);
        return trip.get();
    }
}

