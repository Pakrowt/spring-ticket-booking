package by.samarcev.springsecurity.service;

import by.samarcev.springsecurity.dto.InformationTicketDto;
import by.samarcev.springsecurity.dto.TicketDto;
import by.samarcev.springsecurity.model.Ticket;

import java.util.List;
import java.util.Optional;

public interface TicketService {

    Ticket purchaseTicket(Ticket ticket, Long cardId);

    List<TicketDto> getAllTicketsByUserId(Long userId);

    InformationTicketDto findTicketWithAllInformationAboutTrip(Long ticketId);

}
