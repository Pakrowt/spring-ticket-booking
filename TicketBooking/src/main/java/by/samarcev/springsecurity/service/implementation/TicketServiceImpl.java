package by.samarcev.springsecurity.service.implementation;

import by.samarcev.springsecurity.converter.TicketConverter;
import by.samarcev.springsecurity.dto.InformationTicketDto;
import by.samarcev.springsecurity.dto.TicketDto;
import by.samarcev.springsecurity.exception.NotAvailableSeatException;
import by.samarcev.springsecurity.exception.NotFoundException;
import by.samarcev.springsecurity.model.*;
import by.samarcev.springsecurity.repository.*;
import by.samarcev.springsecurity.service.TicketService;
import by.samarcev.springsecurity.util.MoneyCardUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;
    private final TicketConverter ticketConverter;
    private final TripRepository tripRepository;
    private final MoneyCardRepository moneyCardRepository;
    private final BusRepository busRepository;
    private final UserRepository userRepository;

    @Override
    @Transactional
    public Ticket purchaseTicket(Ticket ticket, Long cardId) {
        MoneyCard moneyCard = moneyCardRepository.findById(cardId)
                .orElseThrow(() -> new NotFoundException("money card", cardId));
        log.info("Card account - {} found", cardId);
        Trip trip = tripRepository.findById(ticket.getTripId().getId())
                .orElseThrow(() -> new NotFoundException("trip", ticket.getTripId().getId()));
        log.info("Trip - {} found", trip);
        existRemainingSeats(ticket);
        existsTicketByTripIdAndSeatNo(ticket);
        BigDecimal costAfterConversion = convertCurrency(moneyCard, trip.getTripCost());
        enoughMoneyForTicket(moneyCard, costAfterConversion);
        log.info("Balance before payment - {}", moneyCard.getBalance());
        moneyCard.setBalance(MoneyCardUtil.reduceMoney(moneyCard.getBalance(), costAfterConversion));
        moneyCardRepository.save(moneyCard);
        log.info("Payment from the card - {}, in the amount- {}", cardId, costAfterConversion);
        log.info("Balance after payment - {}", moneyCard.getBalance());
        Bus bus = trip.getBus();
        bus.setRemainingSeats(bus.getRemainingSeats() - 1);
        log.info("Remaining seats after purchase ticket - {}", bus.getRemainingSeats());
        busRepository.save(bus);
        return ticketRepository.save(ticket);
    }

    @Override
    public List<TicketDto> getAllTicketsByUserId(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("user", userId));
        Optional<List<Ticket>> allTickets = ticketRepository.findAllByUserId(user);
        if (allTickets.isEmpty()) {
            log.warn("not found tickets by userId - {}", userId);
            throw new NotFoundException("ticket", userId);
        }
        return ticketConverter.entityToDto(allTickets.get());
    }

    @Override
    public InformationTicketDto findTicketWithAllInformationAboutTrip(Long ticketId) {
        Ticket ticket = ticketRepository.findById(ticketId).orElseThrow(() -> new NotFoundException("ticket", ticketId));
        log.info("ticket by ticket id - {} found", ticketId);
        Trip trip = tripRepository.findById(ticket.getTripId().getId())
                .orElseThrow(() -> new NotFoundException("trip", ticket.getTripId().getId()));
        log.info("trip - {} found", trip);
        InformationTicketDto informationTicketDto = new InformationTicketDto();
        informationTicketDto.setId(ticket.getId());
        informationTicketDto.setPassengerName(ticket.getPassengerName());
        informationTicketDto.setSeatNo(ticket.getSeatNo());
        informationTicketDto.setDepartureDate(trip.getDepartureDate());
        informationTicketDto.setDepartureLocation(trip.getDepartureLocation().getCity());
        informationTicketDto.setArrivalDate(trip.getArrivalDate());
        informationTicketDto.setArrivalLocation(trip.getArrivalLocation().getCity());
        informationTicketDto.setTripCost(trip.getTripCost());
        log.info("ticketInformationDto - {} created! ", informationTicketDto);
        return informationTicketDto;
    }

    @Transactional
    public void existsTicketByTripIdAndSeatNo(Ticket ticket) {
        boolean existSeatByNumber = ticketRepository.existsTicketByTripIdAndSeatNo(ticket.getTripId(), ticket.getSeatNo());
        if (existSeatByNumber) {
            log.error("seat no - {} is not available!", ticket.getSeatNo());
            throw new NotAvailableSeatException(("seat no " + ticket.getSeatNo() + " is not available"));
        }
        log.info("seat no - {} is available!", ticket.getSeatNo());
    }

    @Transactional
    public void existRemainingSeats(Ticket ticket) {
        Integer currentSeats = busRepository.findCurrentRemainingSeats(ticket.getTripId().getBus().getId());
        if (currentSeats <= 0) {
            log.error("It's not available seats!");
            throw new NotAvailableSeatException(("There are no available seats"));
        }
        log.info("There are available seats on the bus");
    }

    @Transactional
    public void enoughMoneyForTicket(MoneyCard moneyCard, BigDecimal tripCost) {
        BigDecimal balance = moneyCard.getBalance();
        if (balance.compareTo(tripCost) <= 0) {
            log.error("Not enough money for ticket!");
            throw new RuntimeException(("Not enough money for ticket! " +
                    "\nCurrent balance " + moneyCard.getBalance() + " , ticket cost - " + tripCost));
        }
        log.info("There is enough money");
    }

    public BigDecimal convertCurrency(MoneyCard moneyCard, BigDecimal tripCost) {
        Currency currency = moneyCard.getCurrency();
        BigDecimal costAfterConvert;
        BigDecimal usdRate = BigDecimal.valueOf(3.2714);
        BigDecimal eurRate = BigDecimal.valueOf(3.6159);
        BigDecimal rubRate = BigDecimal.valueOf(0.0312);
        switch (currency) {
            case BYN:
                costAfterConvert = tripCost;
                break;
            case USD:
                costAfterConvert = tripCost.divide(usdRate, RoundingMode.HALF_UP);
                break;
            case EUR:
                costAfterConvert = tripCost.divide(eurRate, RoundingMode.HALF_UP);
                break;
            case RUB:
                costAfterConvert = tripCost.divide(rubRate, RoundingMode.HALF_UP);
                break;
            default:
                log.error("Unknown currency - {}", currency);
                throw new RuntimeException("Unknown Currency!");
        }
        return costAfterConvert;
    }
}
