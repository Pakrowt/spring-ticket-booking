package by.samarcev.springsecurity.service;

import by.samarcev.springsecurity.dto.TripDto;
import by.samarcev.springsecurity.model.Trip;

import java.time.LocalDateTime;
import java.util.List;

public interface TripService {

    List<Trip> getAllTripsByArrivalLocation(String arrivalLocation);

    List<Trip> getAllTripsByArrivalLocationAndArrivalDate(String arrivalLocation,
                                                             LocalDateTime arrivalDate);

    List<Trip> getAllTripsByDepartureLocation(String departureLocation);

    List<Trip> getAllTripsByDepartureLocationAndDepartureDate(String departureLocation,
                                                                 LocalDateTime departureDate);

    Trip getTripByBusId(String busNumber);

    List<Trip> getAllTrips(Integer pageNo, Integer pageSize,
                           String sortBy);

    List<Trip> getAllByDepartureDate(LocalDateTime departureDate);

    void addTrip(TripDto tripDto);

    void deleteTrip(Long id);

    void modifyTrip(Long id, TripDto tripDto);

}
