package by.samarcev.springsecurity.service.implementation;

import by.samarcev.springsecurity.converter.UserInformationConverter;
import by.samarcev.springsecurity.exception.NotFoundException;
import by.samarcev.springsecurity.model.Role;
import by.samarcev.springsecurity.model.Status;
import by.samarcev.springsecurity.model.User;
import by.samarcev.springsecurity.repository.RoleRepository;
import by.samarcev.springsecurity.repository.UserRepository;
import by.samarcev.springsecurity.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final UserInformationConverter userInformationConverter;

    @Override
    public void register(User user) {
        Role roleUser = roleRepository.findByName("ROLE_USER");
        List<Role> userRoles = new ArrayList<>();
        userRoles.add(roleUser);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(userRoles);
        user.setStatus(Status.ACTIVE);
        User registeredUser = userRepository.save(user);
        log.info("User  - {} successfully registered", registeredUser);
    }

    @Override
    public List<User> getAll() {
        List<User> result = userRepository.findAll();
        log.info("In base found - {} users", result.size());
        return result;
    }

    @Override
    public Optional<User> findByLogin(String login) {
        Optional<User> maybeUser = userRepository.findByLogin(login);
        if (maybeUser.isEmpty()) {
            log.warn("user with login - {} not found!", login);
            return Optional.empty();
        }
        log.info("In base found - {}", maybeUser);
        return maybeUser;
    }

    @Override
    public void delete(String login) {
        User user = userRepository.findByLogin(login).orElseThrow(() -> new NotFoundException("user", login));
        user.setStatus(Status.DELETED);
        userRepository.save(user);
        log.info("user with login - {} was deleted!", login);
    }

    @Override
    public void updateUserInformation(String login, String firstName, String lastName) {
        User user = userRepository.findByLogin(login).orElseThrow(() -> new NotFoundException("user", login));
        log.info("user {} updates data", user);
        userRepository.updateUserInformation(login, firstName, lastName);
        log.info("user {} successfully updated firstName and/or lastName", user);
        userInformationConverter.entityToDto(user);
    }
}
