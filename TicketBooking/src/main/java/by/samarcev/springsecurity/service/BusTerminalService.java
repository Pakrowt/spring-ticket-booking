package by.samarcev.springsecurity.service;

import by.samarcev.springsecurity.model.BusTerminal;

import java.util.List;

public interface BusTerminalService {

    void addBusTerminal(BusTerminal busTerminal);

    void deleteBusTerminal(String city);

    void modifyBusTerminal(String city, BusTerminal busTerminal);

    List<BusTerminal> getAllBusTerminal(Integer pageNo, Integer pageSize,
                                        String sortBy);

}
