package by.samarcev.springsecurity.service.implementation;

import by.samarcev.springsecurity.exception.EntityExistInDB;
import by.samarcev.springsecurity.exception.NotFoundException;
import by.samarcev.springsecurity.model.Bus;
import by.samarcev.springsecurity.repository.BusRepository;
import by.samarcev.springsecurity.service.BusService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class BusServiceImpl implements BusService {

    private final BusRepository busRepository;

    @Override
    public Bus getBusByNumber(String busNumber) {
        Optional<Bus> bus = busRepository.findById(busNumber);
        if (bus.isEmpty()) {
            log.error("bus - {} not found ", bus);
            throw new NotFoundException("bus", busNumber);
        }
        log.info("bus with bus number - {} successfully found!", busNumber);
        return bus.get();
    }

    @Override
    public List<Bus> getAllBus(Integer pageNo, Integer pageSize,
                               String sortBy) {
        PageRequest pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        Page<Bus> allBus = busRepository.findAllBy(pageable);
        if (allBus.hasNext()) {
            return allBus.getContent();
        } else
            return new ArrayList<>();
    }

    @Override
    public void addBus(Bus bus) {
        existsById(bus.getId());
        busRepository.save(bus);
        getBusByNumber(bus.getId());
        log.info("bus - {} successfully created!", bus);
    }

    @Override
    public void deleteBus(String busNumber) {
        Bus bus = getBusByNumber(busNumber);
        busRepository.delete(bus);
        busRepository.flush();
        log.info("bus - {} successfully deleted!", bus);
    }

    @Override
    public void modifyBus(String busNumber, Bus modifyBus) {
        Bus bus = getBusByNumber(busNumber);
        bus.setModel(modifyBus.getModel());
        bus.setCapacity(modifyBus.getCapacity());
        bus.setRemainingSeats(modifyBus.getRemainingSeats());
        log.info("bus {} successfully updated!", bus);
        busRepository.save(bus);
    }

    public void existsById(String busNumber) {
        if (busRepository.existsById(busNumber)) {
            log.info("bus by bus number {} exist", busNumber);
            throw new EntityExistInDB("bus", busNumber);
        }
        log.info("bus by bus number {} not exist", busNumber);
    }
}
