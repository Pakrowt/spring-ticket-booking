package by.samarcev.springsecurity.service.implementation;

import by.samarcev.springsecurity.exception.EntityExistInDB;
import by.samarcev.springsecurity.exception.NotFoundException;
import by.samarcev.springsecurity.model.BusTerminal;
import by.samarcev.springsecurity.repository.BusTerminalRepository;
import by.samarcev.springsecurity.service.BusTerminalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class BusTerminalServiceImpl implements BusTerminalService {

    private final BusTerminalRepository busTerminalRepository;

    @Override
    public void addBusTerminal(BusTerminal busTerminal) {
        existsInDB(busTerminal.getCity());
        busTerminalRepository.save(busTerminal);
        findTerminalById(busTerminal.getCity());
        log.info("busTerminal - {} successfully created!", busTerminal);
    }

    @Override
    @Modifying
    public void deleteBusTerminal(String city) {
        BusTerminal busTerminal = findTerminalById(city);
        busTerminalRepository.delete(busTerminal);
        busTerminalRepository.flush();
        log.info("busTerminal - {} successfully deleted!", busTerminal);
    }

    @Override
    public void modifyBusTerminal(String city, BusTerminal busTerminal) {
        BusTerminal terminal = findTerminalById(busTerminal.getCity());
        terminal.setCountry(busTerminal.getCountry());
        busTerminalRepository.save(terminal);
        log.info("busTerminal {} successfully updated!", terminal);
    }

    @Override
    public List<BusTerminal> getAllBusTerminal(Integer pageNo, Integer pageSize,
                                               String sortBy) {
        PageRequest pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        log.info("get all bus terminals");
        Page<BusTerminal> allBusTerminals = busTerminalRepository.findAllBy(pageable);
        if (allBusTerminals.hasNext()) {
            return allBusTerminals.getContent();
        } else
            return new ArrayList<BusTerminal>();
    }

    public void existsInDB(String city) {
        if (busTerminalRepository.existsById(city)) {
            log.info("bus terminal {} exist", city);
            throw new EntityExistInDB("busTerminal", city);
        }
        log.info("bus terminal {} not exist", city);
    }

    public BusTerminal findTerminalById(String city) {
        Optional<BusTerminal> terminal = busTerminalRepository.findById(city);
        if (terminal.isEmpty()) {
            log.error("busTerminal - {} not found ", terminal);
            throw new NotFoundException("busTerminal", city);
        }
        log.info("bus terminal - {} successfully found", terminal);
        return terminal.get();
    }
}
