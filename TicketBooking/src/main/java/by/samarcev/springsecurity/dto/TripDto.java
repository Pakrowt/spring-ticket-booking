package by.samarcev.springsecurity.dto;

import by.samarcev.springsecurity.model.TripStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(value = "Dto поездки")
public class TripDto {

    @ApiModelProperty(value = "id поездки (уникальный)", example = "1245")
    private Long id;
    @ApiModelProperty(value = "время отправления автобуса", example = "2022-03-05 16:08:44")
    private LocalDateTime departureDate;
    @ApiModelProperty(value = "город отправления автобуса", example = "Минск")
    private String departureLocation;
    @ApiModelProperty(value = "время прибытия автобуса", example = "2022-03-05 16:08:44")
    private LocalDateTime arrivalDate;
    @ApiModelProperty(value = "город прибытия автобуса", example = "Москва")
    private String arrivalLocation;
    @ApiModelProperty(value = "номер автобуса, который производит поездку", example = "ИИ-1111-1")
    private String busId;
    @ApiModelProperty(value = "статус поездки", example = "SCHEDULED")
    private TripStatus status;
    @ApiModelProperty(value = "стоимость поездки", example = "39.40")
    private BigDecimal tripCost;

}
