package by.samarcev.springsecurity.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "Dto пользователя")
public class UserDto {

    @ApiModelProperty(value = "id пользователя(уникальный)", example = "5")
    private Long id;
    @ApiModelProperty(value = "login пользователя(уникальный)", example = "testuser")
    private String login;
    @ApiModelProperty(value = "имя пользователя", example = "Иван")
    private String firstName;
    @ApiModelProperty(value = "фамилия пользователя", example = "Иванов")
    private String lastName;
    @ApiModelProperty(value = "электронная почта пользователя", example = "ivan-ivanov@mail.ru")
    private String email;

}
