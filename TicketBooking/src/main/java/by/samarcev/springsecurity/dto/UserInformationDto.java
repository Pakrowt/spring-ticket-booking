package by.samarcev.springsecurity.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInformationDto {

    @ApiModelProperty(value = "имя пользователя", example = "Иван")
    private String firstName;
    @ApiModelProperty(value = "фамилия пользователя", example = "Иванов")
    private String lastName;
}
