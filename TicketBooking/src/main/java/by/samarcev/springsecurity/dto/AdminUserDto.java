package by.samarcev.springsecurity.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "Dto пользоваля, которое служит для предоставления администратору основных данных пользователя")
public class AdminUserDto {

    @ApiModelProperty(value = "id пользователя(уникальный)", example = "5")
    private Long id;
    @ApiModelProperty(value = "login пользователя(уникальный)", example = "testuser")
    private String login;
    @ApiModelProperty(value = "Имя пользователя", example = "Ivan")
    private String firstName;
    @ApiModelProperty(value = "Фамилия пользователя", example = "Ivanov")
    private String lastName;
    @ApiModelProperty(value = "email пользователя", example = "ivanov-ivan@mail.ru")
    private String email;
    @ApiModelProperty(value = "Статус пользователя", example = "ACTIVE")
    private String status;
    @ApiModelProperty(value = "Время создания пользователя", example = "2022-03-03 12:52:25")
    private LocalDateTime created;
    @ApiModelProperty(value = "Время обновления данных пользователя", example = "2022-03-03 12:52:26")
    private LocalDateTime updated;
}
