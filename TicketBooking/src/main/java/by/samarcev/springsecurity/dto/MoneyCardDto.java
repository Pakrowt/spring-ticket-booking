package by.samarcev.springsecurity.dto;

import by.samarcev.springsecurity.model.Currency;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "Dto банковской карты")
public class MoneyCardDto {

    @ApiModelProperty(value = "название карты", example = "Visa 5545")
    private String nameCard;
    @ApiModelProperty(value = "id пользователя, который владеет картой", example = "10")
    private Long userId;
    @ApiModelProperty(value = "валюта в которой хранятся деньги", example = "BYN")
    private Currency currency;
    @ApiModelProperty(value = "баланс денежных средств на карте", example = "145.99")
    private BigDecimal balance;

}
