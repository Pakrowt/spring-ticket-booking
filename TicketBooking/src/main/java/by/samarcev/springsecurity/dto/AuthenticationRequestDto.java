package by.samarcev.springsecurity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Dto пользователя, которое содержит данные для авторизации")
public class AuthenticationRequestDto {

    @ApiModelProperty(value = "login пользователя (уникальный)", example = "user")
    private String login;
    @ApiModelProperty(value = "пароль пользователя", example = "password")
    private String password;
}
