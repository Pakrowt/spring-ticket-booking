package by.samarcev.springsecurity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Dto билета на автобус ")
public class TicketDto {

    @ApiModelProperty(value = "id билета (уникальный)", example = "1245")
    private Long id;
    @ApiModelProperty(value = "ФИО человека на которого оформлен билет", example = "Иванов И.И.")
    private String passengerName;
    @ApiModelProperty(value = "id поездки на которую оформлен билет", example = "5")
    private Long tripId;
    @ApiModelProperty(value = "посадочное место в автобусе", example = "6")
    private Integer seatNo;
    @ApiModelProperty(value = "id пользователя с аккаунта которого оформлен билет", example = "999")
    private Long userId;


}
