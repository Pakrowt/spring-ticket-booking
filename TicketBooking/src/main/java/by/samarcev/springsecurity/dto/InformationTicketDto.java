package by.samarcev.springsecurity.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(value = "Dto, которое содержит все данные о билете + данные поездки")
public class InformationTicketDto {

    private Long id;
    private String passengerName;
    private Integer seatNo;
    private LocalDateTime departureDate;
    private String departureLocation;
    private LocalDateTime arrivalDate;
    private String arrivalLocation;
    private BigDecimal tripCost;

}
