package by.samarcev.springsecurity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "Dto, которое служит для пополнения банковского счета")
public class MoneyCardReplenishmentDto {

    @ApiModelProperty(value = "сумма пополнения банковского счета", example = "500.45")
    private BigDecimal amount;
}
