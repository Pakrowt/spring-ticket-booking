package by.samarcev.springsecurity.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MoneyCardUtil {

    public static BigDecimal addMoney(BigDecimal balance, BigDecimal amount) {
        return balance.add(amount).setScale(2, RoundingMode.HALF_UP);
    }

    public static BigDecimal reduceMoney(BigDecimal balance, BigDecimal amount) {
        return balance.subtract(amount).setScale(2, RoundingMode.HALF_UP);
    }
}
