package by.samarcev.springsecurity.util;

import by.samarcev.springsecurity.exception.NotAvailableSeatException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TicketUtil {

    public static void existRemainingSeats(Integer currentSeats) {
        if (currentSeats > 0) {
            log.error("It's not available seats!");
            throw new NotAvailableSeatException(("There are no available seats"));
        }
        log.info("There are available seats on the bus");
    }
}
