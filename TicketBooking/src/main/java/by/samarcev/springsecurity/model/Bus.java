package by.samarcev.springsecurity.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Bus{

    @Id
    @Column(name = "bus_number")
    @NotBlank(message = "Bus number is mandatory")
    @ApiModelProperty(value = "Автобусный номер (уникальный)", example = "KK-2424-5")
    private String id;

    @ApiModelProperty(value = "Модель автобуса", example = "Volkswagen Transporter")
    private String model;

    @Column(name = "total_seats")
    @ApiModelProperty(value = "Общее количество мест в автобусе", example = "16")
    private Integer capacity;

    @Column(name = "remaining_seats")
    @ApiModelProperty(value = "Количество свободных мест в автобусе", example = "2")
    private Integer remainingSeats;

    @OneToOne(mappedBy = "bus", cascade = CascadeType.ALL)
    @JsonIgnore
    private Trip trip;
}

