package by.samarcev.springsecurity.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true, exclude = "user")
@Entity
@Data
@ToString(exclude = "user")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "money_card")
public class MoneyCard extends BaseEntity {

    @Column(name = "name_card")
    @ApiModelProperty(value = "название карты", example = "Visa 5545")
    private String nameCard;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(value = "валюта в которой хранятся деньги", example = "BYN")
    private Currency currency;

    @ApiModelProperty(value = "баланс денежных средств на карте", example = "145.99")
    private BigDecimal balance;

    @JsonBackReference("user-bankAccount")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @ApiModelProperty(value = "id пользователя, который владеет картой", example = "10")
    private User user;

}
