package by.samarcev.springsecurity.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true, exclude = {"roles", "accounts", "ticketSet"})
@Entity
@Data
@ToString(exclude = {"roles", "accounts", "ticketSet"})
@AllArgsConstructor
@NoArgsConstructor
@Builder()
@Table(name = "users")
@EntityListeners(AuditingEntityListener.class)
public class User extends BaseEntity {

    @ApiModelProperty(value = "id пользователя(уникальный)", example = "5")
    private String login;

    @ApiModelProperty(value = "пароль пользователя", example = "password")
    private String password;

    @Column(name = "first_name")
    @ApiModelProperty(value = "имя пользователя", example = "Иван")
    private String firstName;

    @Column(name = "last_name")
    @ApiModelProperty(value = "фамилия пользователя", example = "Иванов")
    private String lastName;

    @ApiModelProperty(value = "электронная почта пользователя", example = "ivan-ivanov@mail.ru")
    private String email;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(value = "Статус пользователя", example = "ACTIVE")
    private Status status;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private List<Role> roles;

    @JsonManagedReference("user-bankAccount")
    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, mappedBy = "user")
    private Set<MoneyCard> accounts;

    @JsonManagedReference("user-tickets")
    @Builder.Default
    @OneToMany(mappedBy = "tripId", cascade = CascadeType.ALL)
    private Set<Ticket> ticketSet = new HashSet<>();

    public void addTicketToUser(Ticket ticket) {
        ticketSet.add(ticket);
        ticket.setUserId(this);
    }

    public void addMoneyCardToUser(MoneyCard moneyCard) {
        accounts.add(moneyCard);
        moneyCard.setUser(this);
    }
}
