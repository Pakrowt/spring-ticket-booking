package by.samarcev.springsecurity.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(exclude = {"bus", "departureLocation", "arrivalLocation", "ticketSet"}, callSuper = true)
@ToString(exclude = {"bus", "departureLocation", "arrivalLocation", "ticketSet"})
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Trip extends BaseEntity {

    @Column(name = "departure_date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "MM/dd/yyyy")
    @ApiModelProperty(value = "время отправления автобуса", example = "2022-03-05 16:08:44")
    private LocalDateTime departureDate;

    @Column(name = "arrival_date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "MM/dd/yyyy")
    @ApiModelProperty(value = "время прибытия автобуса", example = "2022-03-05 16:08:44")
    private LocalDateTime arrivalDate;

    @Enumerated(EnumType.STRING)
    private TripStatus status;

    @Column(name = "trip_cost")
    private BigDecimal tripCost;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "bus_id")
    private Bus bus;

    @JsonBackReference(value = "DepLocation-terminals")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "departure_location")
    @ApiModelProperty(value = "город отправления автобуса", example = "Минск")
    private BusTerminal departureLocation;

    @JsonBackReference(value = "ArrLocation-terminals")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "arrival_location")
    @ApiModelProperty(value = "город прибытия автобуса", example = "Москва")
    private BusTerminal arrivalLocation;

    @JsonManagedReference(value = "ticket-trips")
    @Builder.Default
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "tripId")
    private Set<Ticket> ticketSet = new HashSet<>();

    public void addTicketToTrip(Ticket ticket) {
        ticketSet.add(ticket);
        ticket.setTripId(this);
    }
}
