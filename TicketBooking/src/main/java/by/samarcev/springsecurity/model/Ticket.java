package by.samarcev.springsecurity.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@ToString(exclude = {"userId", "tripId"})
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Ticket extends BaseEntity {

    @Column(name = "passenger_name")
    @ApiModelProperty(value = "ФИО человека на которого оформлен билет", example = "Иванов И.И.")
    private String passengerName;

    @Column(name = "seat_no")
    @ApiModelProperty(value = "посадочное место в автобусе", example = "6")
    private Integer seatNo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    @JsonBackReference(value = "user-tickets")
    @ApiModelProperty(value = "id пользователя с аккаунта которого оформлен билет", example = "999")
    private User userId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "trip_id")
    @JsonBackReference(value = "ticket-trips")
    @ApiModelProperty(value = "id поездки на которую оформлен билет", example = "5")
    private Trip tripId;


}
