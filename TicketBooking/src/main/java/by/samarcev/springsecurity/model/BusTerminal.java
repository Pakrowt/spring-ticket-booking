package by.samarcev.springsecurity.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(exclude = {"departureTripsList", "arrivalTripsList"})
@ToString(exclude = {"departureTripsList", "arrivalTripsList"})
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "bus_terminal")
public class BusTerminal {

    @Id
    @NotBlank(message = "City is mandatory")
    @ApiModelProperty(value = "Название города, где находится вокзал", example = "Минск")
    private String city;

    @ApiModelProperty(value = "Название страны, где находится вокзал", example = "Беларусь")
    private String country;

    @JsonIgnore
    @Builder.Default
    @OneToMany(mappedBy = "departureLocation")
    private Set<Trip> departureTripsList = new HashSet<>();

    @JsonIgnore
    @Builder.Default
    @OneToMany(mappedBy = "arrivalLocation")
    private Set<Trip> arrivalTripsList = new HashSet<>();

    public void addDepartureTripToLocation(Trip trip) {
        departureTripsList.add(trip);
        trip.setDepartureLocation(this);
    }

    public void addArrivalTripToLocation(Trip trip) {
        arrivalTripsList.add(trip);
        trip.setArrivalLocation(this);
    }


}