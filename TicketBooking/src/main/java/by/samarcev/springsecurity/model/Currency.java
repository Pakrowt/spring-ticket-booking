package by.samarcev.springsecurity.model;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "Enum для обозначения валют")
public enum Currency {
    BYN,
    RUB,
    USD,
    EUR
}
