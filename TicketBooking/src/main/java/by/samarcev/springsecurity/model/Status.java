package by.samarcev.springsecurity.model;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "Enum для возможных статусов")
public enum Status {
    ACTIVE,
    NOT_ACTIVE,
    DELETED
}
