package by.samarcev.springsecurity.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
public class Role extends BaseEntity {

    @Column(name = "name")
    @ApiModelProperty(value = "Название роли", example = "ROLE_USER")
    private String name;

    @ApiModelProperty(value = "Статус роли", example = "ACTIVE")
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToMany(mappedBy = "roles", fetch = FetchType.EAGER)
    private List<User> users;
}
