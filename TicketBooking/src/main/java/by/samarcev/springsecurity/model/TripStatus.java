package by.samarcev.springsecurity.model;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "Enum для статусов отправления автобусов")
public enum TripStatus {
    ARRIVED,
    DEPARTED,
    CANCELLED,
    SCHEDULED
}

