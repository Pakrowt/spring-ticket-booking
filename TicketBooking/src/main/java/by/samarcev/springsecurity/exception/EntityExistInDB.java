package by.samarcev.springsecurity.exception;

public class EntityExistInDB extends RuntimeException {

    public EntityExistInDB(String entityClass, Object id) {
        super("Entity " + entityClass + " with id " + id + " exist in DB");
    }
}
