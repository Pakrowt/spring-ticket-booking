package by.samarcev.springsecurity.exception;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String entityClass, Object id) {
        super("Unable to find " + entityClass + " with id " + id);
    }
}
