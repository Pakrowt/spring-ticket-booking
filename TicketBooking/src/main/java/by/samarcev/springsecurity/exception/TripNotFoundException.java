package by.samarcev.springsecurity.exception;

public class TripNotFoundException extends RuntimeException {

    public TripNotFoundException(String condition) {
        super("trips with " + condition + " not found");
    }
}
