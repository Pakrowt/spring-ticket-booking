package by.samarcev.springsecurity.repository;

import by.samarcev.springsecurity.model.MoneyCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Repository
public interface MoneyCardRepository extends JpaRepository<MoneyCard, Long> {

    @Transactional
    @Query("select balance from MoneyCard where id=:cardId")
    BigDecimal checkingBalance(@Param("cardId") Long cardId);

}
