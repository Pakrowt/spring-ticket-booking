package by.samarcev.springsecurity.repository;

import by.samarcev.springsecurity.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByLogin(String login);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update User SET firstName=:firstName, lastName=:lastName WHERE login=:login")
    void updateUserInformation(@Param("login") String login, @Param(value = "firstName") String firstName,
                               @Param(value = "lastName") String lastName);
}
