package by.samarcev.springsecurity.repository;

import by.samarcev.springsecurity.model.Trip;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface TripRepository extends JpaRepository<Trip, Long> {

    Optional<List<Trip>> findAllByArrivalLocation_City(String arrivalLocation);

    Optional<List<Trip>> findAllByArrivalLocation_CityAndArrivalDate(String arrivalLocation,
                                                                     LocalDateTime arrivalDate);

    Optional<List<Trip>> findAllByDepartureLocation_City(String departureLocation);

    Optional<List<Trip>> findAllByDepartureLocation_CityAndDepartureDate(String departureLocation,
                                                                         LocalDateTime departureDate);

    Optional<Trip> findByBusId(String busNumber);

    Page<Trip> findAllBy(Pageable pageable);

    Optional<List<Trip>> findAllByDepartureDate(LocalDateTime departureDate);

}
