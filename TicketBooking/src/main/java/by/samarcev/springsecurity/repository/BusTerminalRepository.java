package by.samarcev.springsecurity.repository;

import by.samarcev.springsecurity.model.BusTerminal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusTerminalRepository extends JpaRepository<BusTerminal, String> {

    Page<BusTerminal> findAllBy(Pageable pageable);
}
