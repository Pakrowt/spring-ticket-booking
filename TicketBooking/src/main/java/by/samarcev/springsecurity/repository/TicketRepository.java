package by.samarcev.springsecurity.repository;

import by.samarcev.springsecurity.dto.InformationTicketDto;
import by.samarcev.springsecurity.model.Ticket;
import by.samarcev.springsecurity.model.Trip;
import by.samarcev.springsecurity.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    boolean existsTicketByTripIdAndSeatNo(Trip trip, Integer seatNo);

    Optional<List<Ticket>> findAllByUserId(User user);

//    @Query(value = "select ti.id, ti.passengerName,ti.seatNo, tr.departureDate, tr.departureLocation," +
//            "tr.arrivalDate, tr.arrivalLocation, tr.tripCost from Ticket ti JOIN Trip tr ON ti.tripId.id=tr.id " +
//            "WHERE ti.id=:ticketId")
//    InformationTicketDto findTicketWithAllInformationAboutTrip(@Param("ticketId") Long ticketId);

}
