package by.samarcev.springsecurity.repository;

import by.samarcev.springsecurity.model.Bus;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BusRepository extends JpaRepository<Bus, String> {

    Page<Bus> findAllBy(Pageable pageable);

    @Query("select remainingSeats from Bus where id=:busNumber")
    Integer findCurrentRemainingSeats(@Param("busNumber") String busNumber);

    boolean existsById(@NotNull String busNumber);
}
