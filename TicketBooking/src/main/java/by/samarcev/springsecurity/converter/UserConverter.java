package by.samarcev.springsecurity.converter;

import by.samarcev.springsecurity.dto.UserDto;
import by.samarcev.springsecurity.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserConverter {

    public UserDto entityToDto(User user) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(user, UserDto.class);
    }

    public List<UserDto> entityToDto(List<User> user) {
        return user.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    public User dtoToEntity(UserDto userDto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(userDto, User.class);
    }

    public List<User> dtoToEntity(List<UserDto> dto) {
        return dto.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
