package by.samarcev.springsecurity.converter;

import by.samarcev.springsecurity.dto.MoneyCardDto;
import by.samarcev.springsecurity.model.MoneyCard;
import by.samarcev.springsecurity.repository.UserRepository;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
@RequiredArgsConstructor
public class MoneyCardConverter {

    private final UserRepository userRepository;

    public MoneyCardDto entityToDto(MoneyCard moneyCard) {
        MoneyCardDto dto = new MoneyCardDto();
        dto.setNameCard(moneyCard.getNameCard());
        dto.setCurrency(moneyCard.getCurrency());
        dto.setBalance(moneyCard.getBalance());
        dto.setUserId(moneyCard.getUser().getId());
        return dto;
    }

    public List<MoneyCardDto> entityToDto(List<MoneyCard> user) {
        return user.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    public MoneyCard dtoToEntity(MoneyCardDto moneyCardDto) {
        MoneyCard moneyCard = new MoneyCard();
        moneyCard.setNameCard(moneyCardDto.getNameCard());
        moneyCard.setCurrency(moneyCardDto.getCurrency());
        moneyCard.setBalance(moneyCardDto.getBalance());
        moneyCard.setUser(userRepository.getById(moneyCardDto.getUserId()));
        return moneyCard;
    }

    public List<MoneyCard> dtoToEntity(List<MoneyCardDto> dto) {
        return dto.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
