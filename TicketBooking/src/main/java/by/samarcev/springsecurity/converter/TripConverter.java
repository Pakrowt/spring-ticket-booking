package by.samarcev.springsecurity.converter;

import by.samarcev.springsecurity.dto.TripDto;
import by.samarcev.springsecurity.model.Trip;
import by.samarcev.springsecurity.repository.BusRepository;
import by.samarcev.springsecurity.repository.BusTerminalRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class TripConverter {

    private final BusRepository busRepository;
    private final BusTerminalRepository busTerminalRepository;

    public TripDto entityToDto(Trip trip) {
        TripDto dto = new TripDto();
        dto.setId(trip.getId());
        dto.setDepartureDate(trip.getDepartureDate());
        dto.setDepartureLocation(trip.getDepartureLocation().getCity());
        dto.setArrivalDate(trip.getArrivalDate());
        dto.setArrivalLocation(trip.getArrivalLocation().getCity());
        dto.setBusId(trip.getBus().getId());
        dto.setStatus(trip.getStatus());
        dto.setTripCost(trip.getTripCost());
        return dto;
    }

    public List<TripDto> entityToDto(List<Trip> user) {
        return user.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    public Trip dtoToEntity(TripDto tripDto) {
        Trip trip = new Trip();
        trip.setId(tripDto.getId());
        trip.setDepartureDate(tripDto.getDepartureDate());
        trip.setDepartureLocation(busTerminalRepository.getById(tripDto.getDepartureLocation()));
        trip.setArrivalDate(tripDto.getArrivalDate());
        trip.setArrivalLocation(busTerminalRepository.getById(tripDto.getArrivalLocation()));
        trip.setBus(busRepository.getById(tripDto.getBusId()));
        trip.setStatus(tripDto.getStatus());
        trip.setTripCost(tripDto.getTripCost());
        return trip;
    }

    public List<Trip> dtoToEntity(List<TripDto> dto) {
        return dto.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
