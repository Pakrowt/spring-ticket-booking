package by.samarcev.springsecurity.converter;

import by.samarcev.springsecurity.dto.UserInformationDto;
import by.samarcev.springsecurity.model.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInformationConverter {

    public UserInformationDto entityToDto(User user) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(user, UserInformationDto.class);
    }

    public List<UserInformationDto> entityToDto(List<User> user) {
        return user.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    public User dtoToEntity(UserInformationDto userInformationDto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(userInformationDto, User.class);
    }

    public List<User> dtoToEntity(List<UserInformationDto> dto) {
        return dto.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
