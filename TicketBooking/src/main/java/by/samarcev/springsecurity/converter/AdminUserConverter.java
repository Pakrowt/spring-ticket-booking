package by.samarcev.springsecurity.converter;

import by.samarcev.springsecurity.dto.AdminUserDto;
import by.samarcev.springsecurity.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AdminUserConverter {

    public AdminUserDto convertToDto(User user) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(user, AdminUserDto.class);
    }

    public User convertToEntity(AdminUserDto adminUserDto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(adminUserDto, User.class);
    }

    public List<AdminUserDto> convertToDto(List<User> user) {
        return user.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    public List<User> convertToEntity(List<AdminUserDto> dto) {
        return dto.stream().map(this::convertToEntity).collect(Collectors.toList());
    }
}
