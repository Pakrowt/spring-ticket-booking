package by.samarcev.springsecurity.converter;

import by.samarcev.springsecurity.dto.TicketDto;
import by.samarcev.springsecurity.model.Ticket;
import by.samarcev.springsecurity.repository.TripRepository;
import by.samarcev.springsecurity.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class TicketConverter {

    private final TripRepository tripRepository;
    private final UserRepository userRepository;

    public TicketDto entityToDto(Ticket ticket) {
        TicketDto dto = new TicketDto();
        dto.setId(ticket.getId());
        dto.setPassengerName(ticket.getPassengerName());
        dto.setTripId(ticket.getTripId().getId());
        dto.setSeatNo(ticket.getSeatNo());
        dto.setUserId(ticket.getUserId().getId());
        return dto;
    }

    public List<TicketDto> entityToDto(List<Ticket> user) {
        return user.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    public Ticket dtoToEntity(TicketDto ticketDto) {
        Ticket ticket = new Ticket();
        ticket.setPassengerName(ticketDto.getPassengerName());
        ticket.setTripId(tripRepository.findById(ticketDto.getTripId()).get());
        ticket.setSeatNo(ticketDto.getSeatNo());
        ticket.setUserId(userRepository.findById(ticketDto.getUserId()).get());
        return ticket;
    }

    public List<Ticket> dtoToEntity(List<TicketDto> dto) {
        return dto.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
